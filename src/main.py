import requests

DATASET_URL = 'https://docs.google.com/spreadsheets/d/1xJqdajXuG8ceFEqF6K3-xlgQuKEPuUEP76m3OObIwtU/gviz/tq?tqx=out:csv&sheet=DebateGovRS_v2'

with open('dataset.csv', 'wb') as fb:
    fb.write(requests.get(DATASET_URL).content)
